#![allow(unused_imports)]
#![feature(try_blocks)]
use anyhow::Context;
use clap::Parser;
use ipfs_api_backend_hyper::response::Cid;
use ipfs_api_backend_hyper::{IpfsApi, IpfsClient};
use serde::Deserialize;
use serde_json::Number;
use serde_json::Value;
use std::collections::HashMap;
use std::path::Path;
use std::path::PathBuf;
// use std::fs::File;
// use color_eyre::eyre::Result;
use anyhow::{anyhow, bail, Result};
use execute::Execute;
use mktemp::Temp;
use std::process::{Command, Stdio};
// use yaml_rust::{YamlLoader, Yaml};
use regex::Regex;
use std::fmt;
// use tokio::main;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Path of flake to build
    #[arg(short, long, default_value = ".")]
    path: PathBuf,

    /// Repeate X time to validate
    #[arg(short, long, default_value_t = 0)]
    repeat: u8,

    /// Run locally instead of bacalhau
    #[arg(short, long, action)]
    local: bool,

    /// Run locally instead of bacalhau
    #[arg(short, long, action)]
    verbose: bool,
}

#[derive(Debug)]
#[allow(dead_code)]
enum BuildRequest {
    Local(PathBuf),
    Repo(String),
}

#[tokio::main]
async fn main() -> Result<()> {
    let args = Args::parse();
    println!("args: {:?}", args);
    let client = IpfsClient::default();

    let inputs: Vec<String> = gather_inputs(&client, &args.path, args.verbose).await?;
    // let inputs = vec![
    //     "QmRGKLig2YTS33tfAJKBQVPSVuejGYLnTcmUdEf4SnBRmq".to_string(),
    //     "QmVGjkrEqC6BJyuKpnWeRusYhjVAkv9zmpxLbmefGJ1HZy".to_string(),
    // ];
    println!("\nInputs: {:#?}", inputs);

    let result_cid = if args.local {
        let output_dir = match build_local(BuildRequest::Local(args.path), args.verbose)
            .await
            .expect("Build should run")
        {
            BuildResult::Success(outputs_dir) => outputs_dir,
            BuildResult::MissingAsset(url) => {
                // let result = build_local(BuildRequest::Repo(url), args.verbose).await;
                // panic!("result: {:?}", result)
                panic!("missing asset: {:?}", url)
            }
        };
        upload(&client, output_dir.as_path())
            .await
            .expect("Upload should work")
    } else {
        let repo_cid = upload(&client, &args.path)
            .await
            .expect("Upload should work");
        println!("repo_cid: {:?}", repo_cid);
        // let job_id = String::from("67a44aa3-0e5d-43f7-8236-4b482cafca4a");
        let job_id = build_bacalhau(repo_cid, inputs, args.verbose)
            .await
            .expect("Build should succeed");
        println!("job_id: {:?}", job_id);
        println!("bacalhau get {}", job_id);

        get_output_cid(job_id)
            .await
            .expect("We should be able to get output CID")
        // println!("output_cid: {:?}", output_cid);
    };

    println!("\nSee logs: https://ipfs.io/ipfs/{}/", result_cid);
    println!(
        "See result: https://ipfs.io/ipfs/{}/outputs/result",
        result_cid
    );
    Ok(())
}

async fn gather_inputs(client: &IpfsClient, path: &PathBuf, verbose: bool) -> Result<Vec<String>> {
    let inputs = nix_query_inputs(path)
        .await
        .context("get inputs from flake metadata")?;
    println!("flake inputs: {:?}", inputs);

    let mut input_cids: Vec<String> = vec![];
    for input in inputs {
        println!("Checking input: {}", input.0);
        let flake_ref = match input
            .1
            .get("type")
            .with_context(|| format!("get type from lock node: {:?}", input.1))?
            .as_str()
            .with_context(|| {
                format!(
                    "get type from lock node as string: {:?}",
                    input.1.get("type")
                )
            })? {
            "github" => Ok(format!(
                "github:{}/{}",
                input
                    .1
                    .get("owner")
                    .context("get repo owner from lock node")?,
                input.1.get("repo").context("get repo from lock node")?
            )),
            _ => Err(anyhow!(
                "Unimplemented lock type: {:?}",
                input.1.get("type")
            )),
        }?;
        let cid = fetch_input(client, &flake_ref, verbose).await?;
        println!("Fetched input {} as {}", flake_ref, cid);
        input_cids.push(cid);
    }
    Ok(input_cids)
}

async fn upload(client: &IpfsClient, path: &Path) -> Result<String> {
    // let file = File::open(path).expect("could not read source file");

    let response = client.add_path(path).await?;
    // println!("response: {:?}", response);
    let last_response = response.last().unwrap();
    // .ok_or(|| anyhow!("no result"))?;
    Ok(last_response.hash.clone())
}

async fn fetch_input(client: &IpfsClient, flake_ref: &String, verbose: bool) -> Result<String> {
    println!("Fetching input: {}", flake_ref);
    let outputs_dir = Temp::new_dir().expect("should be able to create temporary directory");
    let mut cmd = Command::new("docker");
    cmd.arg("run").arg("--rm");
    cmd.arg("-v").arg(format!(
        "{}:/outputs/",
        outputs_dir
            .to_str()
            .expect("should be able to convert tmp dir to string")
    ));
    cmd.arg("registry.gitlab.com/txlab/ipnix:nix");
    cmd.arg("bash");
    cmd.arg("-c");
    cmd.arg(
        [
            String::from("nix profile install nixpkgs#jq"),
            format!(
                "nix flake prefetch {} {} --json | tee /tmp/prefetch.log",
                if verbose { "--debug" } else { "" },
                flake_ref,
            ),
            format!(
                "nix-store {} --export $(nix-store --query --requisites $(jq -r .storePath /tmp/prefetch.log)) > /outputs/export.nar",
                if verbose { "--debug" } else { "" },
            ),
        ]
        .join(" && "),
    );

    println!("Launching prefetcher docker...");
    let result = run_cmd(&mut cmd).await?;

    if verbose && !result.stdout.is_empty() {
        println!("Docker output:\n{}", result.stdout);
    }
    if result.code != 0 {
        bail!("Failed to prefetch {}", flake_ref)
    }
    Ok(upload(client, outputs_dir.as_path())
        .await
        .with_context(|| {
            format!(
                "Uploading prefetch result from {:?}",
                outputs_dir.as_os_str() // .to_str().expect("tmp path is valid unicode")
            )
        })?)
}

// consider: --no-use-registries --no-update-lock-file --debug
fn make_docker_cmd(verbose: bool, flake_ref: &String) -> String {
    format!(
        "nix build --offline --keep-going {} {} -o /tmp/result {} && cp -rL /tmp/result /outputs/ && chown -R --reference /input /outputs && chmod -R u+w /outputs {}",
        if verbose {"--debug"} else {""},
        flake_ref,
        if verbose {"&& ls -al /tmp/result"} else {""},
        if verbose {"&& ls -al /outputs/result"} else {""},
    )
}
// #[derive(Debug, Clone)]
// struct MissingAsset {
//     url: String,
// }
// impl std::error::Error for MissingAsset {}

// impl fmt::Display for MissingAsset {
//     fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
//         write!(f, "Missing asset: {}", self.url)
//     }
// }
#[derive(Debug)]
enum BuildResult {
    Success(Temp),
    MissingAsset(String),
}
// impl fmt::Display for Temp {
//     fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
//         write!(f, "{}", self.as_str())
//     }
// }

async fn build_local(build_request: BuildRequest, verbose: bool) -> Result<BuildResult> {
    let outputs_dir = Temp::new_dir().expect("should be able to create temporary directory");
    let mut cmd = Command::new("docker");
    cmd.arg("run").arg("--rm").arg("--network=none");
    if let BuildRequest::Local(ref path) = build_request {
        cmd.arg("-v").arg(format!(
            "{}:/inputs/:ro",
            path.canonicalize()
                .expect("should be able to canonicalize repo dir")
                .to_str()
                .expect("should be able to convert repo dir to string")
        ));
    }
    cmd.arg("-v").arg(format!(
        "{}:/outputs/",
        outputs_dir
            .to_str()
            .expect("should be able to convert tmp dir to string")
    ));
    cmd.arg("-w");
    cmd.arg("/inputs");
    cmd.arg("registry.gitlab.com/txlab/ipnix:nix");
    cmd.arg("bash");
    cmd.arg("-c");
    cmd.arg(make_docker_cmd(
        verbose,
        &match build_request {
            BuildRequest::Local(_) => ".".to_string(), // as we're in the mounted repo dir, the ref is '.'
            BuildRequest::Repo(url) => url,
        },
    ));

    println!("Launching local docker...");
    // unimplemented!();
    run_cmd(&mut cmd).await.and_then(|result| {
        if verbose && !result.stdout.is_empty() {
            println!("Docker output:\n{}", result.stdout);
        }
        if result.code == 0 {
            Ok(BuildResult::Success(outputs_dir))
        } else {
            let re_missing_url = Regex::new(r"(?m)^error: unable to download '(.+)': ").unwrap();
            let missing_url = re_missing_url
                .captures(result.stderr.as_str())
                .expect("No regex match")
                .get(1)
                .expect("No regex group")
                .as_str()
                .to_string();

            Ok(BuildResult::MissingAsset(missing_url))
        }
    })
}

async fn build_bacalhau(repo_cid: String, inputs: Vec<String>, verbose: bool) -> Result<String> {
    let mut cmd = Command::new("bacalhau");
    cmd.arg("docker");
    cmd.arg("run");
    cmd.arg("--id-only");
    cmd.arg("--wait-timeout-secs").arg("20");
    for (index, input_cid) in inputs.iter().enumerate() {
        cmd.arg("-v").arg(format!("{}:/input/{}", input_cid, index));
    }
    cmd.arg("-v").arg(format!("{}:/flake", repo_cid));
    cmd.arg("-w");
    cmd.arg("/flake");
    cmd.arg("registry.gitlab.com/txlab/ipnix:nix");
    cmd.arg("--");
    // cmd.arg("echo hi");
    cmd.arg("bash");
    cmd.arg("-c");
    cmd.arg(
        [
        [
            "ls -al && echo && ls -al /input/*", 
            "for input in /input/*; do echo 'Importing' $input && nix-store --import < $input/export.nar; done",
            // "nix build  --offline --keep-going --debug . -o /tmp/result; ls -al /tmp/result; cp -rL /tmp/result /outputs/"
            make_docker_cmd(
                verbose, &".".to_string() // as we're in the mounted repo dir, the ref is '.'
            ).as_str()
        ].join(" && "),
        "echo ERROR".into() ].join(" || ") //- workaround for https://github.com/filecoin-project/bacalhau/issues/1504
    );
    // // consider: --no-use-registries --no-update-lock-file
    // cmd.arg("ls -al; nix build  --offline --keep-going --debug . -o /tmp/result; ls -al /tmp/result; cp -rL /tmp/result /outputs/");

    // println!("Launching bacalhau... {:?}", cmd);
    println!("Launching bacalhau...");
    run_cmd(&mut cmd)
        .await
        .map(|result| result.stdout.trim().to_string())
}

async fn get_output_cid(job_id: String) -> Result<String> {
    let mut cmd = Command::new("bacalhau");
    cmd.arg("describe");
    cmd.arg(job_id);
    // cmd.arg("--output-dir");
    // cmd.arg("/tmp/result");

    let output = run_cmd(&mut cmd)
        .await
        .expect("bacalhau describe should execute successfully");

    // HACK: didn't manage yaml stuff, so regex it is
    let result_cid_re = Regex::new(r"(?m)^\s+CID: (.+)$").unwrap();
    Ok(result_cid_re
        .captures(&output.stdout)
        .with_context(|| format!("No regex match for CID in: {}", output.stdout))?
        .get(1)
        .expect("No regex group")
        .as_str()
        .to_string())
    // let parsed = YamlLoader::load_from_str(&output)
    // .expect("Invalid YAML");
    // let nodes = parsed[0]["JobState"]["Nodes"]
    //     .as_hash().expect("as_hash")
    //     .entries();
    // let first_node = nodes.next().expect("first Node");
    // let result_cid = first_node.get()["Shards"];
    // println!("\nresult: {:?}", result_cid);
    // Ok(result_cid)
}

struct CmdResult {
    code: i32,
    stdout: String,
    stderr: String,
}

async fn run_cmd(cmd: &mut Command) -> Result<CmdResult> {
    // println!("Running command: {:?}", cmd);
    cmd.stdout(Stdio::piped());
    cmd.stderr(Stdio::piped());

    let result = cmd
        .execute_output()
        .with_context(|| format!("should be able to execute command {:?}", &cmd))?;
    let exit_code = result.status.code();
    // println!("result: {:?}", result);

    let stdout = String::from_utf8(result.stdout).expect("should be able to UTF8-decode stdout");
    let stderr = String::from_utf8(result.stderr).expect("should be able to UTF8-decode stderr");
    if !stderr.is_empty() {
        println!("\nStderr:\n{}", stderr);
    }
    match exit_code {
        Some(code) => {
            println!("Command finished with exit code {}.", code);

            Ok(CmdResult {
                code: code,
                stderr: stderr.to_string(),
                stdout: stdout.to_string(),
            })
        }
        None => return Err(anyhow!("Job interrupted by signal.")),
    }
}

// #[derive(Deserialize, Debug)]
// struct FlakeMetaOutput {
//     locks: FlakeMetaLocks,
// }
// #[derive(Deserialize, Debug)]
// struct FlakeMetaLocks {
//     nodes: HashMap<String, FlakeMetaLock>,
// }
// #[derive(Deserialize, Debug)]
// struct FlakeMetaLock {
//     locked: FlakeMetaLockRef,
// }
// #[derive(Deserialize, Debug)]
// struct FlakeMetaLockRef {
//     // lastModified: Number
//     // "type": String,
//     narHash: String,
//     owner: String,
//     repo: String,
//     rev: String,
// }

async fn nix_query_inputs(path: &PathBuf) -> Result<Vec<(String, Value)>> {
    let mut cmd = Command::new("nix");
    cmd.arg("flake").arg("metadata").arg("--json").arg(path);
    let result = run_cmd(&mut cmd).await?;
    let value: Value = serde_json::from_str(result.stdout.as_str())
        .with_context(|| format!("Should json deserialize flake metadata: {}", result.stdout))?;
    let inputs: Result<Vec<(String, Value)>> = try {
        let nodes = value
            .get("locks")
            .context("get field: metadata.locks")?
            .get("nodes")
            .context("get field: metadata.locks.nodes")?;
        let lock_nodes = nodes
            .as_object()
            .with_context(|| format!("get as object: metadata.locks.nodes - {:?}", nodes))?;
        lock_nodes
            .clone()
            .into_iter()
            .filter(|entry| entry.0 != "root")
            .map(|entry| {
                // println!("node: {:#?}", entry);
                (
                    entry.0.clone(),
                    entry.1.get("locked").expect("get field 'locked'").clone(),
                )
            })
            .collect::<Vec<(String, Value)>>()
    };

    // println!("Extracted inputs: {:#?}", inputs);
    Ok(inputs
        .with_context(|| format!("Should json deserialize flake metadata: {}", result.stdout))?)
}
