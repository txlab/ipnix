{
  description = "A very basic flake";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/ef2f213d9659a274985778bff4ca322f3ef3ac68";
  };

  outputs = { self, nixpkgs }: {

    packages.hello = with import nixpkgs { system = "x86_64-linux"; }; stdenv.mkDerivation {
      name = "hello";
      src = self;
      installPhase = "mkdir -p $out; echo test content 2 > $out/hello";
    };

    packages.x86_64-linux.default = self.packages.hello;

  };
}
