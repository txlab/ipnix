{
  description = "A very basic flake";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/ef2f213d9659a274985778bff4ca322f3ef3ac68";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }: {

    packages.hello = with import nixpkgs { system = "x86_64-linux"; }; stdenv.mkDerivation {
      name = "hello";
      src = self; #builtins.toFile "hello" "nix";
      installPhase = "mkdir -p $out; echo test import flake utils: " + flake-utils + " > $out/testfile";
    };

    packages.x86_64-linux.default = self.packages.hello;

  };
}
