# CoD-Style Nix builder
## based on IPFS and bacalhau

**Status:** Hack project

Fetches flake inputs to IPFS and then builds the flake via a bacalhau job (which [~~has~~ had](https://bacalhau.substack.com/p/bacalhau-project-report-jan-13-2022) no network access), resulting in a deterministic IPFS output.  
(in the future: might use some form of decentralized consensus or trust network to tell which CID is the result of a flake definition, thus enabling decentralized asset caching)

### How to run:
#### Prerequisites
- local IPFS daemon on default port (`127.0.0.1:5001` = rpc)
- latest [bacalhau cli](https://github.com/filecoin-project/bacalhau/releases/)

#### Steps
1. `git clone https://gitlab.com/txlab/ipnix && cd ipnix/`
2. `direnv allow` (or install rust & stuff on your own)
3. `cargo run -- -vp ./test/nix-with-net/`


## Demo:
[![asciicast](https://asciinema.org/a/YMsBTWJ5aUssRKIwDoKdLssQD.svg)](https://asciinema.org/a/YMsBTWJ5aUssRKIwDoKdLssQD)

[=> Example result Link](https://ipfs.io/ipfs/QmXKjwswzMT19VCQDtedZR63fd1ia3yw7YrZd2ighew2ru/)

### Gotchas / ToDo
- only github as input is currently handled (but adding more http-based ones is easy, and the main nixpkgs repo is github-based)
- building another flake as an input to this flake is not implemented yet
- Upload to IPFS works, but sometimes the IPFS node of bacalhau does not find the blocks in time, and times out. Retrying after some time is the workaround, uploading to estuary is my best idea to fix it.
- ... and a lot more - it's a hacky PoC, as mentioned 😜 (but also pretty cool, huh? 😎)